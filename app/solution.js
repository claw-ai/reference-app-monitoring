/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions for calling solution APIs
*/


function getSolutionsForOrg() {
       

var orgId = $('#divSelectOrg').find('#selectOrg').val();

           
//Checks to ensure this function is called with a valid Org or Exits
 if (!orgId) {
        console.log("unable to process solution for orgID = " + orgId);  
        return;
    }


    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/solutions',
        //GET /v1/orgs/{orgId}/solutions
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {

            // Create an html option for each map entry found.
            var items = $.map(result, function (solItem, index) {
                var optionItem = $('<option value="' + solItem.id + '">' + solItem.name + '</option>')
                return optionItem;
            });

            var selectSolution = $('<select id="selectSolution" size="1" style="width: 400px;"></select>');
            selectSolution.append(items);

            console.log("Solution Items = " + items); 

            // Show the list of solutions
            $('#divSelectSolution').show();
            $('#divSolutionList').html(selectSolution);
            $('#divSolutionList').show();

            //Prepare the History Button and Screen for History 
            resetMonitoringUI(); 
           
           
            // Wire up the click handler for this list box to reset the UX on change
           
            $('#selectSolution').on('click', function () {
                    resetMonitoringUI(); 
            });
            
            
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error getting the solutions org: ' + orgId + ' - ' + errorThrown + '</p>');
            var divFailure = $('#divSelectSolution').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}


