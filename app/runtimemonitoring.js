/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions for calling APIs for runtime and monitoring
*/



/* Helper function to hide monitoring related UI when selections are changed
*/
function resetMonitoringUI() {
    var divSuccess = $('#divShowSolutionHistory').find('.alert-success');
    var divFailure = $('#divShowSolutionHistory').find('.alert-danger');
    divSuccess.empty();
    divSuccess.slideUp();
    divFailure.empty();
    divFailure.slideUp();

    $('#divHistoryResults').slideUp();
}

/* Displays the solution history in table.  Includes status icons based on the status of each record.
   Calculates processing time and total records.
 */
function showSolutionHistory() {
    resetMonitoringUI();
     $('#solution-history-wait').show();
    var activeSolutionId = $('#selectSolution').val();
    var orgId = $('#selectOrg').val();
    console.log('Getting solution history for solutionId: ', activeSolutionId);
   $('#divHistoryResults').slideDown();

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/solutions/' + activeSolutionId + '/history',
        // GET v1/orgs/{orgId}/solutions/{solutionId}/history
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log(result);

            if (result.length > 0) {
                //  Build an html for the validation results
                var sourceRows = $.map(result, function (historyItem, index) {
                    var cssClass;
                    var tblRow = $('<tr></tr>');

                    // Show alternating row colors
                    if (index % 2)
                        cssClass = "";
                    else
                        cssClass = "alt-row";

                    //Start
                    $('<td class="' + cssClass + ' col-xs-2">' + moment(historyItem.start).format('M/D/YYYY h:mm A') + '</td>').appendTo(tblRow);
                    //Duration
                    $('<td class="' + cssClass + ' col-xs-1">' + moment().startOf('day').add(historyItem.duration, 'minutes').format('m:ss') + '</td>').appendTo(tblRow);
                    //Records/Sec
                    var totalProcessingTimeSec = moment.duration(historyItem.duration, 'minutes').asSeconds();
                    var totalRecs = historyItem.recordsProcessed + historyItem.recordsFailed;
                    $('<td class="' + cssClass + ' col-xs-1">' + Math.round10((totalRecs / totalProcessingTimeSec), -3) + '</td>').appendTo(tblRow);

                    //Status
                    var resultTD = $('<td class="' + cssClass + ' col-xs-4"></td>');
                    resultTD.attr('title', historyItem.details);
                    // Set the result text
                    var spanHistory = $('<span></span>');
                    resultWithSpaces = historyItem.result.replace(/([A-Z])/g, ' $1').trim();
                    spanHistory.html(resultWithSpaces);

                    // Set the status icon
                    var imgStatus = $('<img class="statusIcon"></img>');
                    if (historyItem.result == 'FatalError')
                        imgStatus.attr('src', 'img/icon-status-alert.png');
                    else if (historyItem.result == 'CompletedSuccessfully')
                        imgStatus.attr('src', 'img/icon-status-ready.png');
                    else
                        imgStatus.attr('src', 'img/icon-status-warning.png');

                    imgStatus.appendTo(resultTD);
                    spanHistory.appendTo(resultTD);
                    resultTD.appendTo(tblRow);

                    //Recs Processed
                    $('<td class="' + cssClass + ' col-xs-2">' + historyItem.recordsProcessed + '</td>').appendTo(tblRow);
                    //Recs Failed
                    $('<td class="' + cssClass + ' col-xs-1">' + historyItem.recordsFailed + '</td>').appendTo(tblRow);
                    //Pending
                    $('<td class="' + cssClass + ' col-xs-1">' + historyItem.reprocessRecordsRemaining + '</td>').appendTo(tblRow);

                    return tblRow;
                });

                // Show the table of org and solution information
                $("#tbl-solution-history tbody").html(sourceRows);
                $('#divHistoryResults').slideDown();

                msg = "Showing history for solutionId: " + activeSolutionId;
            }
            else
                msg = "There wasn't any history information found for solutionId: " + activeSolutionId;

            // Show success message
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divShowSolutionHistory').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
            $('#solution-history-wait').hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error getting the solution history for solutionId: ' + activeSolutionId + ' - ' + errorThrown + '</p>');
            var divFailure = $('#divShowSolutionHistory').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
            $('#solution-history-wait').hide();
        }
    });

}
